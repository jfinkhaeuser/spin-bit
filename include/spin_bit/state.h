/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef SPIN_BIT_STATE_H
#define SPIN_BIT_STATE_H

#ifndef __cplusplus
#error This is a C++, header-only library.
#endif

#include <chrono>

namespace spin_bit {

/**
 * The core of the spin bit functionality, enhanced with a Valid Edge Counter
 * (VEC), is reading and modifying state. The spin bit itself is, as the name
 * suggests, a single bit - here provided as a boolean value. The VEC consists
 * of two bits. We assume it is a container (such as a plain array or
 * std::bitset) that can be accessed via an index. We further store a packet
 * number, which can be any numerical value, and a time stamp. To provide
 * flexibility, all these are template paramters for a state structure.
 */
template <
  typename spin_bitT,
  typename vecT,
  typename packet_numberT,
  typename clockT = std::chrono::steady_clock,
  typename timeT = typename clockT::time_point,
  typename durationT = typename clockT::duration,
  std::enable_if_t<
    std::is_same<
      timeT,
      std::chrono::time_point<clockT, durationT>
    >::value,
    bool
  > = true
>
struct state
{
  // Scoped types; aliases of template paramters
  using spin_bit_t = spin_bitT;
  using vec_t = vecT;
  using packet_number_t = packet_numberT;

  // Time-related scoped types
  using time_point = timeT;
  using duration = durationT;
  using clock = clockT;

  // State values
  spin_bit_t      spin_bit = {};
  vec_t           vec = {};
  packet_number_t packet_number = {};
  time_point      time = {};
  bool            new_edge = false;
};

} // namespace spin_bit

#endif // guard
