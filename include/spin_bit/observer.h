/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef SPIN_BIT_OBSERVER_H
#define SPIN_BIT_OBSERVER_H

#ifndef __cplusplus
#error This is a C++, header-only library.
#endif

#include <cstdint>
#include <chrono>
#include <memory>
#include <tuple>

#include <spin_bit/vec.h>

namespace spin_bit {

/**
 * Observers observe spin bit and optional VEC information in both flow
 * directions. The definition of what constitutes a flow is outside of
 * this code's scope; it's just interested in knowing about the direction
 * in which some packet data was observed.
 *
 * Note: From an in-path observer's point of view, packets from the client
 *       to the server are upstream, and responses are downstream. The
 *       observer can, however, be implmented in either the client or the
 *       server, in which case it would distinguish between ingress and egress
 *       flows. A client's egress flow is an in-path's observers upstream
 *       direction, but a server's ingress flow (and vice versa), so it's
 *       not really possible to provide a definitive mapping of upstream/
 *       downstream meanings to egress/ingress. The equivalency below is
 *       purely for easier use of the same code in all three observer
 *       positions.
 *
 * Note: Observers that are not in-path will have negligible up- or downstream
 *       delays, and measure the RTT mostly on the other sample.
 *
 *       The general definition is:
 *       | Spin bit value | Direction | Interpretation |
 *       | same           | same      | no edge        |
 *       | different      | same      | Full RTT       |
 *       | same           | different | Half RTT       |
 *       | different      | different | error          |
 */
enum flow_direction : uint8_t
{
  DIR_UPSTREAM = 0,
  DIR_INGRESS = DIR_UPSTREAM,
  DIR_INCOMING = DIR_INGRESS,

  DIR_DOWNSTREAM = 1,
  DIR_EGRESS = DIR_DOWNSTREAM,
  DIR_OUTGOING = DIR_EGRESS,
};


/**
 * RTT can be measured in half samples by the observers, because they observe
 * each flow direction (somewhat) independently. The following specifies
 * whether RTT could be sampled already, half-sampled or fully sampled.
 */
enum sample_type : uint8_t
{
  SAMPLE_NO_EDGE      = 0,
  SAMPLE_HALF_RTT     = 1,
  SAMPLE_FULL_RTT     = 2,
  SAMPLE_ERROR_RESET  = 3,
};


/**
 * Individual evaluations/observations of a spin bit and flow direction. These
 * are re-used in different observers.
 */
template <typename stateT>
struct observer_operations
{
public:
  using spin_bit_t = typename stateT::spin_bit_t;
  using vec_t = typename stateT::vec_t;

  using time_point = typename stateT::time_point;
  using duration = typename stateT::duration;
  using clock = typename stateT::clock;

  using result = std::tuple<sample_type, flow_direction, duration>;

  // | Spin bit value | Direction | Interpretation |
  // | same           | same      | no edge        |
  // | different      | same      | Full RTT       |
  // | same           | different | Half RTT       |
  // | different      | different | error          |


  inline std::tuple<bool, result>
  is_edge_start(flow_direction dir, spin_bit_t spin_bit)
  {
    // Initialize if we haven't taken any samples yet
    if (edge_start == time_point{}) {
      // First sample; initialize state and return NO_EDGE
      edge_start = clock::now();
      value = spin_bit;
      direction = dir;

      return {true, {SAMPLE_NO_EDGE, dir, {}}};
    }
    return {false, {}};
  }

  inline std::tuple<bool, result>
  is_no_edge(flow_direction dir, spin_bit_t spin_bit)
  {
    // If spin bit and direction are the same, we have no edge.
    if (spin_bit == value && direction == dir) {
      edge_current = clock::now();

      return {true, {SAMPLE_NO_EDGE, dir, {}}};
    }
    return {false, {}};
  }

  inline std::tuple<bool, result>
  is_full_rtt(flow_direction dir, spin_bit_t spin_bit)
  {
    // If the spin bit is different but the direction is the same, we have
    // a full RTT.
    if (spin_bit != value && direction == dir) {
      // Take a sample
      edge_current = clock::now();
      auto sample = edge_current - edge_start;

      // Initialize for the next edge
      edge_start = edge_current;
      value = spin_bit;

      return {true, {SAMPLE_FULL_RTT, dir, sample}};
    }
    return {false, {}};
  }

  inline std::tuple<bool, result>
  is_half_rtt(flow_direction dir, spin_bit_t spin_bit)
  {
    // If the spin bit is the same, but the direction is different, we can
    // report a half RTT. Note that if there is a burts of packets before
    // a response occurs (a delayed edge), this will return ever increasing
    // half RTT measures.
    if (spin_bit == value && direction != dir) {
      // We can take a sample and return a half edge, but let's not update
      // the direction.
      edge_current = clock::now();
      auto sample = edge_current - edge_start;

      return {true, {SAMPLE_HALF_RTT, dir, sample}};
    }
    return {false, {}};
  }


  inline result
  error_reset(flow_direction dir, spin_bit_t spin_bit)
  {
    edge_start = clock::now();
    edge_current = time_point{};
    value = spin_bit;
    direction = dir;

    std::cerr << "Detected an error, resetting." << std::endl;
    return {SAMPLE_ERROR_RESET, dir, {}};
  }

private:
  time_point      edge_start = {};
  time_point      edge_current = {};
  spin_bit_t      value = {};
  flow_direction  direction = {};
};


/**
 * The basic observer just observes the spin bit and thus a transition edge;
 * it therefore can get two RTT samples (one in each direction) per flow per
 * RTT.
 *
 * It is, however, vulnerable to re-ordering and loss, and cannot detect
 * delayed edges. It should be seen primarily as the base line against which
 * to test other observers, and not be used in production.
 */
template <typename stateT>
struct basic_observer
  : public observer_operations<stateT>
{
public:
  using ops = observer_operations<stateT>;

  using spin_bit_t = typename stateT::spin_bit_t;
  using vec_t = typename stateT::vec_t;
  using packet_number_t = typename stateT::packet_number_t;

  using time_point = typename stateT::time_point;
  using duration = typename stateT::duration;
  using clock = typename stateT::clock;

  using result = std::tuple<sample_type, flow_direction, duration>;

  inline typename ops::result
  observe(flow_direction dir, spin_bit_t spin_bit,
      vec_t const & vec [[maybe_unused]] = {},
      packet_number_t const & packet_number [[maybe_unused]] = {})
  {
    // Initialize if we haven't taken any samples yet
    {
      auto [done, res] = ops::is_edge_start(dir, spin_bit);
      if (done) {
        return res;
      }
    }

    // If spin bit and direction are the same, we have no edge.
    {
      auto [done, res] = ops::is_no_edge(dir, spin_bit);
      if (done) {
        return res;
      }
    }

    // If the spin bit is different but the direction is the same, we have
    // a full RTT.
    {
      auto [done, res] = ops::is_full_rtt(dir, spin_bit);
      if (done) {
        return res;
      }
    }

    // If the spin bit is the same, but the direction is different, we can
    // report a half RTT. Note that if there is a burts of packets before
    // a response occurs (a delayed edge), this will return ever increasing
    // half RTT measures.
    {
      auto [done, res] = ops::is_half_rtt(dir, spin_bit);
      if (done) {
        return res;
      }
    }

    // Finally, the spin bit and direction both are different, which is an
    // error. This indicates that one side detected an edge and responded
    // accordingly, but the other side may not - or lost packets, etc. In this
    // basic observer, the best we can do is reset and report we're lost
    return ops::error_reset(dir, spin_bit);
  }
};



/**
 * The VEC observer observes the spin bit and VEC; it does not know the packet
 * number. Using the VEC makes it unvulnerable to reordering and loss.
 */
template <typename stateT>
struct vec_observer
  : public observer_operations<stateT>
{
public:
  using ops = observer_operations<stateT>;

  using spin_bit_t = typename stateT::spin_bit_t;
  using vec_t = typename stateT::vec_t;
  using packet_number_t = typename stateT::packet_number_t;

  using time_point = typename stateT::time_point;
  using duration = typename stateT::duration;
  using clock = typename stateT::clock;

  inline typename ops::result
  observe(flow_direction dir, spin_bit_t spin_bit,
      vec_t const & vec = {},
      packet_number_t const & packet_number [[maybe_unused]] = {})
  {
    auto vec_value = vec_ops<vec_t>::value(vec);

    // Initialize if we haven't taken any samples yet
    if (vec_value == 0) {
      return {SAMPLE_NO_EDGE, dir, {}};
    }

    // If the spin bit is different but the direction is the same, we have
    // a full RTT.
    if (vec_value >= 3) {
      auto [done, res] = ops::is_full_rtt(dir, spin_bit);
      if (done) {
        return res;
      }
    }

    // If the spin bit is the same, but the direction is different, we can
    // report a half RTT. Note that if there is a burts of packets before
    // a response occurs (a delayed edge), this will return ever increasing
    // half RTT measures.
    if (vec_value >= 2) {
      auto [done, res] = ops::is_half_rtt(dir, spin_bit);
      if (done) {
        return res;
      }
    }


    // If spin bit and direction are the same, we have no edge.
    if (vec_value >= 1) {
      auto [done, res] = ops::is_edge_start(dir, spin_bit);
      if (done) {
        return res;
      }
    }

    // Finally, the spin bit and direction both are different, which is an
    // error. This indicates that one side detected an edge and responded
    // accordingly, but the other side may not - or lost packets, etc. In this
    // basic observer, the best we can do is reset and report we're lost
    return ops::error_reset(dir, spin_bit);
  }
};


} // namespace spin_bit

#endif // guard
