/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef SPIN_BIT_PACKET_H
#define SPIN_BIT_PACKET_H

#ifndef __cplusplus
#error This is a C++, header-only library.
#endif

#include <spin_bit/vec.h>
#include <spin_bit/state.h>

namespace spin_bit {


/**
 * On incoming packets, we expect state to modify. We also expect some
 * information from the incoming packet, namely the spin bit that's set
 * there, the VEC that's set there, and a packet number.
 */
template <typename stateT>
inline bool
on_incoming_packet(stateT & state,
    typename stateT::spin_bit_t spin_bit,
    typename stateT::vec_t const & vec,
    typename stateT::packet_number_t const & packet_number)
{
  // A transition occurs if the spin bit is different, and the packet number
  // is higher.
  if (state.spin_bit == spin_bit || packet_number <= state.packet_number) {
    // No state transition
    return false;
  }

  state.spin_bit = spin_bit;
  state.vec[0] = vec[0];
  state.vec[1] = vec[1];
  state.packet_number = packet_number;
  state.time = stateT::clock::now();
  state.new_edge = true;

  return true;
}


/**
 * On outgoing packets, we produce a spin bit and vector to put into the next
 * packet. The function is behaving slightly differently depending on whether
 * it acts as a client or server (i.e. one party must start new edges, the
 * other must echo back edges). This is determined via a compile-time parameter.
 */
namespace detail {

template <typename spin_bitT, bool CLIENT>
struct spin_bit_setter;

template <typename spin_bitT>
struct spin_bit_setter<spin_bitT, true>
{
  static inline spin_bitT
  set_from(spin_bitT const & value)
  {
    return !value;
  }
};

template <typename spin_bitT>
struct spin_bit_setter<spin_bitT, false>
{
  static inline spin_bitT
  set_from(spin_bitT const & value)
  {
    return value;
  }
};


} // namespace detail

template <typename stateT, bool CLIENT>
inline void
on_outgoing_packet(stateT & state,
    typename stateT::spin_bit_t & spin_bit,
    typename stateT::vec_t & vec,
    typename stateT::duration const & delta)
{
  // Set the spin bit; whether this is the same or the inverse of the state
  // depends on whether this is run in client or server mode.
  spin_bit = detail::spin_bit_setter<
    typename stateT::spin_bit_t, CLIENT
  >::set_from(state.spin_bit);

  // We change the VEC only if this is a new edge.
  if (state.new_edge) {
    auto diff = stateT::clock::now() - state.time;
    if (diff < delta) {
      // This is an edge and not delayed.
      vec[0] = state.vec[0];
      vec[1] = state.vec[1];
      vec_ops<typename stateT::vec_t>::inc(vec);
    }
    else {
      // This is a delayed edge
      vec_ops<typename stateT::vec_t>::one(vec);
    }
  }
  else {
    // This is not an edge; reset
    vec_ops<typename stateT::vec_t>::zero(vec);
  }

  // In either case, we'll not treat this as a new edge now.
  state.new_edge = false;
}

} // namespace spin_bit

#endif // guard
