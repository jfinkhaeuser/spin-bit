/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef SPIN_BIT_VEC_H
#define SPIN_BIT_VEC_H

#ifndef __cplusplus
#error This is a C++, header-only library.
#endif

#include <cstdint>
#include <bitset>

namespace spin_bit {

/**
 * The Vector Edge Counter (VEC) is a two-bit numeric value, equivalent
 * to integers in the range [0, 3]. The following class offers a simple set
 * of operations for handing VECs. An optimiziation for std::bitset<2> is also
 * provided.
 */
template <typename vecT>
struct vec_ops
{
  using vec_t = vecT;

  static inline void from(vec_t & out, uint8_t in)
  {
    out[0] = in & 1 ? 1 : 0;
    out[1] = in & 2 ? 1 : 0;
  }

  static inline uint8_t value(vec_t const & in)
  {
    uint8_t ret = (int(in[0]) * 1) | (int(in[1]) * 2);
    return ret;
  }

  static inline void zero(vec_t & inout)
  {
    inout[0] = inout[1] = 0;
  }

  static inline void one(vec_t & inout)
  {
    inout[0] = 1;
    inout[1] = 0;
  }

  static inline void two(vec_t & inout)
  {
    inout[0] = 0;
    inout[1] = 1;
  }

  static inline void three(vec_t & inout)
  {
    inout[0] = inout[1] = 1;
  }

  static inline void inc(vec_t & inout)
  {
    vec_t max;
    three(max);
    auto maxvalue = value(max);

    auto numeric = value(inout) + 1;
    if (numeric > maxvalue) {
      numeric = maxvalue;
    }
    from(inout, numeric);
  }
};


template <>
struct vec_ops<std::bitset<2>>
{
  using vec_t = std::bitset<2>;

  static inline void from(vec_t & out, uint8_t in)
  {
    out = vec_t{static_cast<unsigned long>(in)};
  }

  static inline uint8_t value(vec_t const & in)
  {
    return in.to_ulong();
  }

  static inline void zero(vec_t & inout)
  {
    inout = vec_t{0UL};
  }

  static inline void one(vec_t & inout)
  {
    inout = vec_t{1UL};
  }

  static inline void two(vec_t & inout)
  {
    inout = vec_t{2UL};
  }

  static inline void three(vec_t & inout)
  {
    inout = vec_t{3UL};
  }

  static inline void inc(vec_t & inout)
  {
    auto val = inout.to_ulong() + 1;
    if (val > 3) {
      val = 3;
    }
    inout = vec_t{val};
  }
};

} // namespace spin_bit

#endif // guard
