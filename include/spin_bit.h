/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef SPIN_BIT_H
#define SPIN_BIT_H

#ifndef __cplusplus
#error This is a C++, header-only library.
#endif

#include <cstdint>

#include <spin_bit/vec.h>
#include <spin_bit/state.h>
#include <spin_bit/packet.h>
#include <spin_bit/observer.h>

#endif // guard
