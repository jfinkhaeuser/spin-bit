# spin-bit

Tiny C++ header implementation of spin bit based RTT measurements in network traffic.

This library is inspired by QUIC, and based on [Piet De Vaere's master thesis](https://pub.tik.ee.ethz.ch/students/2017-HS/MA-2017-16.pdf)
(a copy can be found in the [docs/](./docs/) folder for archiving purposes).
