/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <vector>

#include <spin_bit/vec.h>

template <typename vecT>
inline void
test_vec()
{
  using vec_ops = spin_bit::vec_ops<vecT>;

  // Zero
  {
    typename vec_ops::vec_t v;
    vec_ops::zero(v);
    ASSERT_EQ(0, vec_ops::value(v));
  }

  // One
  {
    typename vec_ops::vec_t v;
    vec_ops::one(v);
    ASSERT_EQ(1, vec_ops::value(v));
  }

  // Two
  {
    typename vec_ops::vec_t v;
    vec_ops::two(v);
    ASSERT_EQ(2, vec_ops::value(v));
  }

  // Three
  {
    typename vec_ops::vec_t v;
    vec_ops::three(v);
    ASSERT_EQ(3, vec_ops::value(v));
  }

  // From 42; goes by bits, and 42 has 2 bit set.
  {
    typename vec_ops::vec_t v;
    vec_ops::from(v, 42);
    ASSERT_EQ(2, vec_ops::value(v));
  }

  // Increment
  {
    typename vec_ops::vec_t v;
    vec_ops::zero(v);
    ASSERT_EQ(0, vec_ops::value(v));
    vec_ops::inc(v);
    ASSERT_EQ(1, vec_ops::value(v));
    vec_ops::inc(v);
    ASSERT_EQ(2, vec_ops::value(v));
    vec_ops::inc(v);
    ASSERT_EQ(3, vec_ops::value(v));

    // Increment again, nothing shooudl happen.
    vec_ops::inc(v);
    ASSERT_EQ(3, vec_ops::value(v));
  }
}

TEST(VEC, with_bool_array)
{
  test_vec<bool [2]>();
}


TEST(VEC, with_int_array)
{
  test_vec<int [2]>();
}


TEST(VEC, with_bitset)
{
  test_vec<std::bitset<2>>();
}
