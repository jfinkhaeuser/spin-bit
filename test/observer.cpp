/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <thread>

#include <spin_bit/observer.h>
#include <spin_bit/state.h>
#include <spin_bit/packet.h> // FIXME

#if defined(DEBUG)
#define LOG_COUNTS(res) std::cerr << (res) << std::endl;
#else
#define LOG_COUNTS(res)
#endif

namespace {

static constexpr std::chrono::microseconds TEST_DELAY{10};

// For modelling a flow, each entry consists of a direction, a spin bit,
// and a VEC value. We also may have access to a proper packet number.
struct flow_entry
{
  spin_bit::flow_direction  direction;
  bool                      spin_bit;
  bool                      vec[2];
  int                       packet_number;
};


static flow_entry STREAM_ORDERED_BURSTLESS[] = {
  { spin_bit::DIR_UPSTREAM,   true,  { true, false }, 1 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, true }, 1 },
  { spin_bit::DIR_UPSTREAM,   false, { true, true },  2 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, true },  2 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, true },  3 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, true },  3 },
  { spin_bit::DIR_UPSTREAM,   false, { true, true },  4 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, true },  4 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, true },  5 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, true },  5 },
};

static flow_entry STREAM_ORDERED_BURSTY[] = {
  { spin_bit::DIR_UPSTREAM,   true,  { true, false },   1 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  2 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  3 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  4 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, true },   1 },
  { spin_bit::DIR_UPSTREAM,   false, { true, true },    5 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false },  6 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false },  7 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, true },    2 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  3 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  4 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  5 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, true },    8 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  9 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, true },    6 },
  { spin_bit::DIR_UPSTREAM,   false, { true, true },   10 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false }, 11 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false }, 12 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, true },    7 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, true },   13 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false }, 14 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false }, 15 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, true },    8 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  9 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 10 },
};

static flow_entry STREAM_ORDERED_BURSTY_WITH_DELAYS[] = {
  { spin_bit::DIR_UPSTREAM,   true,  { true, false },   1 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  2 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, true },   1 },
  { spin_bit::DIR_UPSTREAM,   false, { true, true },    3 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, true },    2 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  3 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  4 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  5 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, true },    4 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  5 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  6 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  7 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, true },    6 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  7 },
  { spin_bit::DIR_UPSTREAM,   false, { true, true },    8 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false },  9 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, true },    8 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, true },   10 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false }, 11 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, true },    9 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 10 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 11 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 12 },
};

static flow_entry STREAM_ORDERED_BURSTY_WITH_REORDERING[] = {
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  2 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, false },   1 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  2 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, false },   1 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false },  4 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false },  5 },
  { spin_bit::DIR_UPSTREAM,   false, { true, false },   3 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  6 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  4 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  5 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, false },   3 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  7 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, false },   6 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  8 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  7 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 11 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 10 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, true },   8 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  9 },
  { spin_bit::DIR_UPSTREAM,   false, { true, false },   9 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false }, 14 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false }, 15 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, true },  12 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false }, 13 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, false },  10 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false }, 12 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false }, 11 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 18 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 17 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, true },  16 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 20 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 19 },
};

static flow_entry STREAM_ORDERED_BURSTY_WITH_LOSS[] = {
  { spin_bit::DIR_UPSTREAM,   true,  { true, false },   1 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  2 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false },  3 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, true },   1 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  2 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  3 },
  { spin_bit::DIR_UPSTREAM,   false, { true, true },    4 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false },  5 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false },  6 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false },  7 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, true },    4 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false },  5 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, true },    8 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, true },    6 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  7 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  8 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false },  9 },
  { spin_bit::DIR_UPSTREAM,   false, { true, true },    9 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false }, 10 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false }, 11 },
  { spin_bit::DIR_UPSTREAM,   false, { false, false }, 12 },
  { spin_bit::DIR_DOWNSTREAM, false, { true, true },   10 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false }, 11 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false }, 12 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false }, 13 },
  { spin_bit::DIR_DOWNSTREAM, false, { false, false }, 14 },
  { spin_bit::DIR_UPSTREAM,   true,  { true, true },   13 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false }, 14 },
  { spin_bit::DIR_UPSTREAM,   true,  { false, false }, 15 },
  { spin_bit::DIR_DOWNSTREAM, true,  { true, true },   15 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 16 },
  { spin_bit::DIR_DOWNSTREAM, true,  { false, false }, 17 },
};

struct observer_counts
{
  size_t  no_edge = 0;
  size_t  half_rtt = 0;
  size_t  full_rtt = 0;
  size_t  error = 0;
};

inline std::ostream &
operator<<(std::ostream & os, observer_counts const & counts)
{
  os << "no_edge: " << counts.no_edge << " - "
    << "half_rtt: " << counts.half_rtt << " - "
    << "full_rtt: " << counts.full_rtt << " - "
    << "error: " << counts.error;
  return os;
}


template <typename observerT>
inline observer_counts
observe_stream(flow_entry * stream, size_t stream_size)
{
  observer_counts counts;
  observerT obs;

  for (size_t i = 0 ; i < stream_size ; ++i) {
    flow_entry & entry = stream[i];
    auto [type, dir, duration] = obs.observe(entry.direction, entry.spin_bit,
        entry.vec, entry.packet_number);
    std::cerr << "type: " << type << " dur: " << duration.count() << std::endl;

    switch (type) {
      case spin_bit::SAMPLE_NO_EDGE:
        ++(counts.no_edge);
        break;

      case spin_bit::SAMPLE_HALF_RTT:
        ++(counts.half_rtt);
        break;

      case spin_bit::SAMPLE_FULL_RTT:
        ++(counts.full_rtt);
        break;

      case spin_bit::SAMPLE_ERROR_RESET:
      default:
        ++(counts.error);
        break;
    }
  }

  LOG_COUNTS(counts);
  return counts;
}


template <typename observerT>
inline void
test_full_rtt(typename observerT::spin_bit_t initial_value,
    spin_bit::flow_direction initial_direction)
{
  // A full RTT requires:
  // - one spin bit value to upstream
  // - the same spin bit value to downstream
  // - a different spin bit value to upstream

  observerT obs;

  {
    auto [type, dir, duration] = obs.observe(initial_direction, initial_value);
    ASSERT_EQ(spin_bit::SAMPLE_NO_EDGE, type);
    ASSERT_EQ(initial_direction, dir);
    ASSERT_EQ(typename observerT::duration{}, duration);
  }

  std::this_thread::sleep_for(TEST_DELAY);

  {
    auto direction = spin_bit::DIR_UPSTREAM == initial_direction
      ? spin_bit::DIR_DOWNSTREAM
      : spin_bit::DIR_UPSTREAM;

    auto [type, dir, duration] = obs.observe(direction, initial_value);
    ASSERT_EQ(spin_bit::SAMPLE_HALF_RTT, type);
    ASSERT_EQ(direction, dir);
    ASSERT_GT(duration, TEST_DELAY);
  }

  std::this_thread::sleep_for(TEST_DELAY);

  {
    auto [type, dir, duration] = obs.observe(initial_direction, !initial_value);
    ASSERT_EQ(spin_bit::SAMPLE_FULL_RTT, type);
    ASSERT_EQ(initial_direction, dir);
    ASSERT_GT(duration, TEST_DELAY * 2);
  }
}

} // anonymous namespace


TEST(BasicObserver, initial)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using observer = spin_bit::basic_observer<state>;

  observer obs;

  auto [type, dir, duration] = obs.observe(spin_bit::DIR_UPSTREAM, true);
  ASSERT_EQ(spin_bit::SAMPLE_NO_EDGE, type);
  ASSERT_EQ(spin_bit::DIR_UPSTREAM, dir);
  ASSERT_EQ(typename state::duration{}, duration);
}


TEST(BasicObserver, no_edge)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using observer = spin_bit::basic_observer<state>;

  observer obs;

  {
    auto [type, dir, duration] = obs.observe(spin_bit::DIR_UPSTREAM, true);
    ASSERT_EQ(spin_bit::SAMPLE_NO_EDGE, type);
    ASSERT_EQ(spin_bit::DIR_UPSTREAM, dir);
    ASSERT_EQ(typename state::duration{}, duration);
  }

  {
    auto [type, dir, duration] = obs.observe(spin_bit::DIR_UPSTREAM, true);
    ASSERT_EQ(spin_bit::SAMPLE_NO_EDGE, type);
    ASSERT_EQ(spin_bit::DIR_UPSTREAM, dir);
    ASSERT_EQ(typename state::duration{}, duration);
  }
}



TEST(BasicObserver, full_rtt)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using observer = spin_bit::basic_observer<state>;

  // Test with different initial values and directions
  test_full_rtt<observer>(true, spin_bit::DIR_UPSTREAM);
  test_full_rtt<observer>(false, spin_bit::DIR_UPSTREAM);
  test_full_rtt<observer>(true, spin_bit::DIR_DOWNSTREAM);
  test_full_rtt<observer>(false, spin_bit::DIR_DOWNSTREAM);
}


TEST(BasicObserver, stream_ordered_burstless)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::basic_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTLESS,
      sizeof(STREAM_ORDERED_BURSTLESS) / sizeof(flow_entry));

  ASSERT_EQ(1, counts.no_edge);
  ASSERT_EQ(5, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}


TEST(BasicObserver, stream_ordered_bursty)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::basic_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTY,
      sizeof(STREAM_ORDERED_BURSTY) / sizeof(flow_entry));

  ASSERT_EQ(11, counts.no_edge);
  ASSERT_EQ(10, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}


TEST(BasicObserver, stream_ordered_bursty_with_delays)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::basic_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTY_WITH_DELAYS,
      sizeof(STREAM_ORDERED_BURSTY_WITH_DELAYS) / sizeof(flow_entry));

  ASSERT_EQ(7, counts.no_edge);
  ASSERT_EQ(12, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}


TEST(BasicObserver, stream_ordered_bursty_with_reordering)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::basic_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTY_WITH_REORDERING,
      sizeof(STREAM_ORDERED_BURSTY_WITH_REORDERING) / sizeof(flow_entry));

  ASSERT_EQ(8, counts.no_edge);
  ASSERT_EQ(20, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}


TEST(BasicObserver, stream_ordered_bursty_with_loss)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::basic_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTY_WITH_LOSS,
      sizeof(STREAM_ORDERED_BURSTY_WITH_LOSS) / sizeof(flow_entry));

  ASSERT_EQ(11, counts.no_edge);
  ASSERT_EQ(17, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}




TEST(VECObserver, initial)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using observer = spin_bit::vec_observer<state>;

  observer obs;

  auto [type, dir, duration] = obs.observe(spin_bit::DIR_UPSTREAM, true);
  ASSERT_EQ(spin_bit::SAMPLE_NO_EDGE, type);
  ASSERT_EQ(spin_bit::DIR_UPSTREAM, dir);
  ASSERT_EQ(typename state::duration{}, duration);
}


TEST(VECObserver, no_edge)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using observer = spin_bit::vec_observer<state>;

  observer obs;

  {
    auto [type, dir, duration] = obs.observe(spin_bit::DIR_UPSTREAM, true);
    ASSERT_EQ(spin_bit::SAMPLE_NO_EDGE, type);
    ASSERT_EQ(spin_bit::DIR_UPSTREAM, dir);
    ASSERT_EQ(typename state::duration{}, duration);
  }

  {
    auto [type, dir, duration] = obs.observe(spin_bit::DIR_UPSTREAM, true);
    ASSERT_EQ(spin_bit::SAMPLE_NO_EDGE, type);
    ASSERT_EQ(spin_bit::DIR_UPSTREAM, dir);
    ASSERT_EQ(typename state::duration{}, duration);
  }
}



TEST(VECObserver, full_rtt)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using observer = spin_bit::vec_observer<state>;

  // Unlike the basic observer, the VEC observer needs to see a VEC, too.
  observer obs;

  {
    auto [type, dir, duration] = obs.observe(spin_bit::DIR_UPSTREAM, true, { true, false });
    ASSERT_EQ(spin_bit::SAMPLE_NO_EDGE, type);
    ASSERT_EQ(spin_bit::DIR_UPSTREAM, dir);
    ASSERT_EQ(typename observer::duration{}, duration);
  }

  std::this_thread::sleep_for(TEST_DELAY);

  {
    auto [type, dir, duration] = obs.observe(spin_bit::DIR_DOWNSTREAM, true, { false, true });
    ASSERT_EQ(spin_bit::SAMPLE_HALF_RTT, type);
    ASSERT_EQ(spin_bit::DIR_DOWNSTREAM, dir);
    ASSERT_GT(duration, TEST_DELAY);
  }

  std::this_thread::sleep_for(TEST_DELAY);

  {
    auto [type, dir, duration] = obs.observe(spin_bit::DIR_UPSTREAM, false, { true, true });
    ASSERT_EQ(spin_bit::SAMPLE_FULL_RTT, type);
    ASSERT_EQ(spin_bit::DIR_UPSTREAM, dir);
    ASSERT_GT(duration, TEST_DELAY * 2);
  }

}


TEST(VECObserver, stream_ordered_burstless)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::vec_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTLESS,
      sizeof(STREAM_ORDERED_BURSTLESS) / sizeof(flow_entry));

  // The VEC observer can't do any better (nor should do worse!) than the
  // basic observer here.
  ASSERT_EQ(1, counts.no_edge);
  ASSERT_EQ(5, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}


TEST(VECObserver, stream_ordered_bursty)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::vec_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTY,
      sizeof(STREAM_ORDERED_BURSTY) / sizeof(flow_entry));

  ASSERT_EQ(16, counts.no_edge);
  ASSERT_EQ(5, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}


TEST(VECObserver, stream_ordered_bursty_with_delays)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::vec_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTY_WITH_DELAYS,
      sizeof(STREAM_ORDERED_BURSTY_WITH_DELAYS) / sizeof(flow_entry));

  ASSERT_EQ(14, counts.no_edge);
  ASSERT_EQ(5, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}


TEST(VECObserver, stream_ordered_bursty_with_reordering)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::vec_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTY_WITH_REORDERING,
      sizeof(STREAM_ORDERED_BURSTY_WITH_REORDERING) / sizeof(flow_entry));

  ASSERT_EQ(23, counts.no_edge);
  ASSERT_EQ(3, counts.half_rtt);
  ASSERT_EQ(0, counts.full_rtt);
  ASSERT_EQ(6, counts.error);
}


TEST(VECObserver, stream_ordered_bursty_with_loss)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    int
  >;
  using observer = spin_bit::vec_observer<state>;

  auto counts = observe_stream<observer>(STREAM_ORDERED_BURSTY_WITH_LOSS,
      sizeof(STREAM_ORDERED_BURSTY_WITH_LOSS) / sizeof(flow_entry));

  ASSERT_EQ(23, counts.no_edge);
  ASSERT_EQ(5, counts.half_rtt);
  ASSERT_EQ(4, counts.full_rtt);
  ASSERT_EQ(0, counts.error);
}
