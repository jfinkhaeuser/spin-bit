/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <thread>
#include <random>

#include <deque>

#include <spin_bit/generator.h>

#if defined(DEBUG)
#define LOG_META(dir, meta) \
    std::cerr << ((dir) \
        ? "client -> server" \
        : "server -> client") \
      << " (PN: " << (meta).packet_number << ") " \
      << "SP: " << (meta).spin_bit << " - VEC: " \
      << static_cast<size_t>((meta).vec_value()) \
      << std::endl;
#define LOG_HEADER(msg) std::cerr << (msg) << std::endl;
#else
#define LOG_META(dir, meta)
#define LOG_HEADER(msg)
#endif



namespace {

static constexpr std::chrono::milliseconds TEST_TIMEOUT{10};


template <typename outT, typename inT>
inline std::vector<typename outT::packet_metadata_t>
burst(outT & out, inT & in, std::size_t const & num,
    std::size_t delay_factor = 1, bool reorder = false,
    bool delay_is_loss = false)
{
  bool do_delay = (num % delay_factor > 0);

  std::vector<typename outT::packet_metadata_t> buf;
  buf.resize(num);
  for (std::size_t i = 0 ; i < num ; ++i) {
    auto [success, meta] = out.produce();
    EXPECT_TRUE(success);
    buf[i] = meta;
  }

  // Reordering is relatively brutal, it reorders all packes.
  if (reorder) {
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(buf.begin(), buf.end(), g);
  }

  // After they're generated, send them to the server to consume
  for (std::size_t i = 0 ; i < num ; ++i) {
    bool do_consume = true;
    if (do_delay && (i % delay_factor)) {
      if (delay_is_loss) {
        do_consume = false;
      }
      else {
        std::this_thread::sleep_for(TEST_TIMEOUT * 2);
      }
    }

    if (do_consume) {
      in.consume(buf[i]);
    }
  }

  return buf;
}


/**
 * The test transport generates a random amount of packets, with a given
 * minimum and maximum amount, before letting the other side consume this
 * state. It then switches to producing packets in the opposide direction.
 * It proceeds with this until each side has transmitted N bursts.
 */
template <typename client_genT, typename server_genT>
inline std::vector<
  std::tuple<bool, typename client_genT::packet_metadata_t>
>
transport(client_genT & client, server_genT & server,
    std::size_t min_packets, std::size_t max_packets, std::size_t num_bursts,
    std::size_t delay_factor = 1,
    bool reorder = false, bool delay_is_loss = false)
{
  std::vector<
    std::tuple<bool, typename client_genT::packet_metadata_t>
  > result;

  // Normalize inputs
  if (min_packets < 1) {
    min_packets = 1;
  }
  if (max_packets < min_packets) {
    max_packets = min_packets;
  }
  if (num_bursts < 1) {
    num_bursts = 1;
  }


  std::default_random_engine rng{
    static_cast<std::size_t>(std::chrono::system_clock::now().time_since_epoch().count())
  };
  std::uniform_int_distribution<std::size_t> dist{min_packets, max_packets};

  // Create a number of bursts.
  for (std::size_t b = 0 ; b < num_bursts ; ++b) {
    auto num = dist(rng);
    // std::cerr << "client -> server (" << num << ")" << std::endl;
    auto out = burst(client, server, num, delay_factor, reorder, delay_is_loss);
    for (auto & meta : out) {
      result.push_back({true, meta});
    }

    num = dist(rng);
    // std::cerr << "server -> client (" << num << ")" << std::endl;
    auto in = burst(server, client, num, delay_factor, reorder, delay_is_loss);
    for (auto & meta : in) {
      result.push_back({false, meta});
    }
  }

  return result;
}


} // anonymous namespace


TEST(Generator, client_produce_simple)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using client_generator = spin_bit::generator<state, true>;

  client_generator client{
    TEST_TIMEOUT, 42,
    [](uint32_t prev) { return prev + 2; }
  };

  // The first produced metadata must be the initial packet number, and signal
  // the start of a stream.
  {
    auto [success, meta] = client.produce();
    ASSERT_TRUE(success);
    ASSERT_EQ(42, meta.packet_number);
    ASSERT_TRUE(meta.spin_bit);
    ASSERT_EQ(0b01, meta.vec_value());
  }

  // Creating a second means the packet number must be incremented by two,
  // and the spin bit must remain the same. The VEC also remains the same,
  // because we have no spin edge.
  {
    auto [success, meta] = client.produce();
    ASSERT_TRUE(success);
    ASSERT_EQ(44, meta.packet_number);
    ASSERT_TRUE(meta.spin_bit);
    ASSERT_EQ(0b00, meta.vec_value());
  }
}



TEST(Generator, server_produce_simple)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using packet_metadata = spin_bit::packet_metadata<state>;
  using server_generator = spin_bit::generator<state, false>;

  server_generator server{
    TEST_TIMEOUT, 42,
    [](uint32_t prev) { return prev + 2; }
  };

  // A server can't produce metadata until it's received a packet.
  {
    auto [success, meta] = server.produce();
    ASSERT_FALSE(success);
  }

  // Let the server observe an initial packet. The packet's spin_bit should be
  // set, but the VEC should be zero.
  packet_metadata initial;
  initial.spin_bit = true;
  initial.packet_number = 123;
  ASSERT_TRUE(server.consume(initial));

  // We should now be able to generate a response packet. The packet number is
  // the initial one. The spin_bit is reflected, so true. The VEC is incremented
  // so 1, as if it was delayed.
  {
    auto [success, meta] = server.produce();
    ASSERT_TRUE(success);
    ASSERT_EQ(42, meta.packet_number);
    ASSERT_TRUE(meta.spin_bit);
    ASSERT_EQ(0b01, meta.vec_value());
  }

  // Generating a second packet should succeed, increment the packet value by two,
  // and reset the VEC to zero, indicating this is not a spin edge.
  {
    auto [success, meta] = server.produce();
    ASSERT_TRUE(success);
    ASSERT_EQ(44, meta.packet_number);
    ASSERT_TRUE(meta.spin_bit);
    ASSERT_EQ(0b00, meta.vec_value());
  }
}



TEST(Generator, generate_stream_ordered_burstless)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using client_generator = spin_bit::generator<state, true>;
  using server_generator = spin_bit::generator<state, false>;

  client_generator client{TEST_TIMEOUT};
  server_generator server{TEST_TIMEOUT};

  auto res = transport(client, server, 1, 1, 5);

  // In an ordered, burstless stream, we expect the server packet to
  // mirror the previous client spin bit
  // flip direction or spin bit.
  bool spin_bit = false;
  LOG_HEADER("STREAM_ORDERED_BURSTLESS");
  for (auto & [dir, meta] : res) {
    LOG_META(dir, meta);

    if (dir) {
      spin_bit = meta.spin_bit;
    }
    else {
      ASSERT_EQ(spin_bit, meta.spin_bit);
    }

    // We also can expect the VEC to never be 0
    ASSERT_NE(0, meta.vec_value());
  }
}


TEST(Generator, generate_stream_ordered_bursty)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using client_generator = spin_bit::generator<state, true>;
  using server_generator = spin_bit::generator<state, false>;

  client_generator client{TEST_TIMEOUT};
  server_generator server{TEST_TIMEOUT};

  auto res = transport(client, server, 1, 5, 5);

  // With bursts, the spin bit still needs to be reflecte, but the VEC
  // needs to be 0 if the direction didn't change.
  bool direction = false; // init to false to expect a non-zero VEC
  bool spin_bit = false;
  LOG_HEADER("STREAM_ORDERED_BURSTY");
  for (auto & [dir, meta] : res) {
    LOG_META(dir, meta);

    if (dir) {
      spin_bit = meta.spin_bit;
    }
    else {
      ASSERT_EQ(spin_bit, meta.spin_bit);
    }

    if (dir != direction) {
      ASSERT_NE(0, meta.vec_value());
    }
    else {
      ASSERT_EQ(0, meta.vec_value());
    }
    direction = dir;
  }
}


TEST(Generator, generate_stream_ordered_bursty_with_delays)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using client_generator = spin_bit::generator<state, true>;
  using server_generator = spin_bit::generator<state, false>;

  client_generator client{TEST_TIMEOUT};
  server_generator server{TEST_TIMEOUT};

  auto res = transport(client, server, 1, 5, 5, 2);

  // With bursts, the spin bit still needs to be reflecte, but the VEC
  // needs to be 0 if the direction didn't change.
  bool direction = false; // init to false to expect a non-zero VEC
  bool spin_bit = false;
  std::size_t delayed = 0;
  LOG_HEADER("STREAM_ORDERED_BURSTY_WITH_DELAYS");
  for (auto & [dir, meta] : res) {
    LOG_META(dir, meta);

    if (dir) {
      spin_bit = meta.spin_bit;
    }
    else {
      ASSERT_EQ(spin_bit, meta.spin_bit);
    }

    if (dir != direction) {
      ASSERT_NE(0, meta.vec_value());

      // We can't guarantee delayed edges with randomness, but if there are
      // any, we can count them.
      if (1 == meta.vec_value()) {
        ++delayed;
      }
    }
    else {
      ASSERT_EQ(0, meta.vec_value());
    }
    direction = dir;
  }

  // The first edge is delayed by definition.
  ASSERT_GE(delayed, 1);
}


TEST(Generator, generate_stream_ordered_bursty_with_reordering)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using client_generator = spin_bit::generator<state, true>;
  using server_generator = spin_bit::generator<state, false>;

  client_generator client{TEST_TIMEOUT};
  server_generator server{TEST_TIMEOUT};

  auto res = transport(client, server, 1, 5, 5, 1, true);

  LOG_HEADER("STREAM_ORDERED_BURSTY_WITH_REORDERING");
  for (auto & [dir, meta] : res) {
    LOG_META(dir, meta);
  }
}


TEST(Generator, generate_stream_ordered_bursty_with_loss)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;
  using client_generator = spin_bit::generator<state, true>;
  using server_generator = spin_bit::generator<state, false>;

  client_generator client{TEST_TIMEOUT};
  server_generator server{TEST_TIMEOUT};

  auto res = transport(client, server, 1, 5, 5, 2, false, true);

  LOG_HEADER("STREAM_ORDERED_BURSTY_WITH_LOSS");
  for (auto & [dir, meta] : res) {
    LOG_META(dir, meta);
  }
}
