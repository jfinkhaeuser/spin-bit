/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <thread>

#include <spin_bit/packet.h>

template <typename T>
class Outgoing : public ::testing::Test {};
TYPED_TEST_SUITE_P(Outgoing);


TYPED_TEST_P(Outgoing, client_no_new_edge)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;
  s.new_edge = false;

  bool spin_bit = false;
  TypeParam vec;
  vec[0] = 1;
  vec[1] = 1;

  spin_bit::on_outgoing_packet<state, true>(s, spin_bit, vec, {});

  ASSERT_TRUE(spin_bit);
  ASSERT_FALSE(vec[0]);
  ASSERT_FALSE(vec[1]);

  ASSERT_FALSE(s.new_edge);
}


TYPED_TEST_P(Outgoing, client_delayed_edge)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;
  s.new_edge = true;
  s.time = state::clock::now();

  // For a delayed edge, we need to have a time diff larger than the duration
  // we're passing to the function. The easiest way is to sleep for twice that
  // duration.
  auto delta = std::chrono::microseconds{20};
  std::this_thread::sleep_for(delta * 2);

  bool spin_bit = false;
  TypeParam vec;
  vec[0] = 1;
  vec[1] = 1;

  spin_bit::on_outgoing_packet<state, true>(s, spin_bit, vec, delta);

  ASSERT_TRUE(spin_bit);
  ASSERT_TRUE(vec[0]);
  ASSERT_FALSE(vec[1]);

  ASSERT_FALSE(s.new_edge);
}


TYPED_TEST_P(Outgoing, client_regular_edge)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;
  s.new_edge = true;
  s.time = state::clock::now();

  // For a regular edge, we want  low delay - as low as zero is ok.
  auto delta = std::chrono::seconds{20};

  bool spin_bit = false;
  TypeParam vec;
  vec[0] = 0;
  vec[1] = 0;

  // First, this should increment the edge we have.
  spin_bit::on_outgoing_packet<state, true>(s, spin_bit, vec, delta);

  ASSERT_TRUE(spin_bit);
  ASSERT_TRUE(vec[0]);
  ASSERT_FALSE(vec[1]);

  ASSERT_FALSE(s.new_edge);

  // We should be able to increment the VEC further if we set the new_edge flag
  s.new_edge = true;
  spin_bit::on_outgoing_packet<state, true>(s, spin_bit, vec, delta);

  ASSERT_TRUE(spin_bit);
  ASSERT_FALSE(vec[0]);
  ASSERT_TRUE(vec[1]);

  ASSERT_FALSE(s.new_edge);

  // Also up to three.
  s.new_edge = true;
  spin_bit::on_outgoing_packet<state, true>(s, spin_bit, vec, delta);

  ASSERT_TRUE(spin_bit);
  ASSERT_TRUE(vec[0]);
  ASSERT_TRUE(vec[1]);

  ASSERT_FALSE(s.new_edge);

  // At this point, there should be no more change
  s.new_edge = true;
  spin_bit::on_outgoing_packet<state, true>(s, spin_bit, vec, delta);

  ASSERT_TRUE(spin_bit);
  ASSERT_TRUE(vec[0]);
  ASSERT_TRUE(vec[1]);

  ASSERT_FALSE(s.new_edge);
}




REGISTER_TYPED_TEST_SUITE_P(Outgoing,
    client_no_new_edge,
    client_delayed_edge,
    client_regular_edge
);

typedef ::testing::Types<
  bool [2],
  std::bitset<2>
> test_types;
INSTANTIATE_TYPED_TEST_SUITE_P(spin_bit, Outgoing, test_types);
