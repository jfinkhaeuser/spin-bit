[MA-2017-16.pdf](MA-2017-16.pdf) is Piet De Vaere's master thesis. Neither they
nor their work are part of this project; this copy is retained for archival
purposes only.
